module.exports = {
  extends: ['airbnb-typescript/base', 'prettier'],
  plugins: ['prettier'],
  env: {
    es6: true,
    node: true,
  },
  parserOptions: {
    ecmaVersion: 2018,
    ecmaFeatures: {},
    sourceType: 'module', // Required with typescript-eslint-parser 22.x
    project: 'tsconfig.json',
  },
  rules: {
    '@typescript-eslint/no-unused-vars': 'error',
    '@typescript-eslint/type-annotation-spacing': 'error',
    'object-curly-newline': ['error', { consistent: true }],
    'prettier/prettier': ['error'],
  },
};
