# Simple API

Simple API for any backend application

## Starting the API

### Quickstart

```bash
yarn install
yarn run-db
yarn start
```

\*optional: use a log formatter like: `yarn start | pino-pretty`

**Dependencies:**

1. NodeJS (version 12.13)
1. Docker

### Configuration

**node-config**
see defaults in `/config/default.ts`  
create any local overrides in `/config/local-development.ts`

## Contributing

### Style

Using prettier and eslint should be automatic via husky and `yarn lint` (or `yarn lint-fix`)  
Your editor should also be able to leverage the `.prettierrc.toml` and `eslintrc.js`

### Testing

The buld executes `yarn test` to please make sure all tests pass.  
We're currently using AVA and NYC/Istambul for unit tests and coverage.

## Deployment

All deployments are managed by GitlabCI and configured in `.gitlab-ci.yml`
