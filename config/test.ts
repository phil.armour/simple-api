module.exports = {
  server: {
    port: '8080',
  },
  db: {
    host: 'localhost',
    port: 5432,
    username: 'root',
    password: 'test',
    database: 'app',
  },
};
