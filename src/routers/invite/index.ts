import express from 'express';
import { createInvite, getInvite } from './controller';

const router = express.Router();
router.post('/', createInvite);
router.get('/:code', getInvite);

export default router;
