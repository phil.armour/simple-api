import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

@Entity('card_invite')
export default class CardInvite extends BaseEntity {
  constructor(email: string, message: string) {
    super();
    this.email = email;
    this.message = message;
    // default values:
    this.inviteCode = uuidv4();
    this.createdAt = new Date();
  }

  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ name: 'invite_code', nullable: false, unique: true, length: 36 })
  inviteCode: string;

  @Column({ length: 254, nullable: false })
  email: string;

  @Column({ length: 500, nullable: true })
  message: string;

  @Column('timestamptz', { name: 'created_at', nullable: false })
  createdAt: Date;

  static findByCode(inviteCode: string) {
    return this.findOne({
      where: { inviteCode },
    });
  }
}
