import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('profile')
export default class Profile extends BaseEntity {
  constructor(email: string) {
    super();
    this.firstName = '';
    this.lastName = '';
    this.email = email;
  }

  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id?: number;

  @Column({ length: 254, nullable: false })
  email: string;

  @Column({ name: 'first_name', length: 30, default: '' })
  firstName: string;

  @Column({ name: 'last_name', length: 150, default: '' })
  lastName: string;

  static findById(profileId: number) {
    return this.findOne({
      where: { id: profileId },
    });
  }
}
