import express from 'express';
import bodyParser from 'body-parser';
import inviteRouter from './routers/invite';

export default () => {
  const app = express();
  app.use(bodyParser.json());
  app.use('/api/invite', inviteRouter);
  return app;
};
