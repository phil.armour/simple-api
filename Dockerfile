FROM node:12.13-alpine

ENV TS_NODE_FILES true
ENV LAST_UPDATED=2019-09-26
ENV WORKDIR=/opt/app
ENV USER=1000
ENV YARN_VERSION 1.19.1
ENV PORT 8080

EXPOSE 8080

RUN apk --no-cache --update upgrade && \
    apk add --no-cache --update \
      ca-certificates \
      curl \
      g++ \
      gcc \
      git \
      make \
      patch \
      python \
      tar && \
    update-ca-certificates && \
    rm -rf /var/cache/apk/* && \
    curl -fSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz" && \
    tar -xzf yarn-v$YARN_VERSION.tar.gz -C /opt/ && \
    ln -snf /opt/yarn-v$YARN_VERSION/bin/yarn /usr/local/bin/yarn && \
    ln -snf /opt/yarn-v$YARN_VERSION/bin/yarnpkg /usr/local/bin/yarnpkg &&\
    rm yarn-v$YARN_VERSION.tar.gz && \
    echo "root:"`< /dev/urandom tr -dc _A-Z-a-z-0-9!@#$%\&\^\(\) | head -c${1:-32};echo;` | chpasswd

WORKDIR ${WORKDIR}

COPY package.json yarn.lock ${WORKDIR}/

RUN chown ${USER}:${USER} ${WORKDIR}

USER ${USER}

RUN yarn --frozen-lockfile --ignore-engines && \
    mkdir -p "${WORKDIR}/node_modules/.cache" && \
    chmod 777 "${WORKDIR}/node_modules/.cache" && \
    rm -f .npmrc

COPY . ${WORKDIR}/

USER 0

RUN apk del \
      g++ \
      gcc \
      git \
      patch

USER ${USER}

ARG VERSION
ARG GITSHA
ENV IMAGE_VERSION=${VERSION}
ENV IMAGE_GITSHA=${GITSHA}

CMD [ "./node_modules/.bin/ts-node", "-T", "index.ts" ]